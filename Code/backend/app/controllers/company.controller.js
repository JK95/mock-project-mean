const LOGGER = require('../../config/project.logger');
const httpMessage = require('../helpers/messages/http.message');
const companyModel = require('../models/company.model');

exports.addCompany = (request, response, next) => {
    try {
        // Checking if method is allowed
        if( !(request.route.methods.post) || request.method != "POST" ) {
            return response.status(405).send(httpMessage[405]);
        }
        // Request body
        let body = request.body;
        // Validation
        if (body == null || body == "" || body == undefined) {
            return response.status(400).send(httpMessage[400]);
        }
        if ((body.firstName == ""|| body.firstName == null || body.firstName == undefined) ||
         (body.email == ""|| body.email == null || body.email == undefined)) {
            return response.status(400).send(httpMessage[400]);
        }
        // Checking if email taken or company already exists
        companyModel.findOne({Email: body.email}).then(findData => {
            if (findData) {
                return response.status(409).send(httpMessage[409]);
            }
            let date = new Date();
            let YY = date.getFullYear();
            let MM = date.getMonth() + 1;
            MM = ((MM<10)?'0':'') + MM;
            let DD = ((date.getDate()<10)?'0':'') + date.getDate();
            let HH = ((date.getHours()<10)?'0':'') + date.getHours();
            let II = ((date.getMinutes()<10)?'0':'') + date.getMinutes();
            let SS = date.getSeconds();
            date = `${YY}/${MM}/${DD} ${HH}:${II}:${SS}`;
            // New company data
            let newCompany = new companyModel({
                Firstname: body.firstName,
                Lastname: body.lastName,
                Email: body.email,
                Phone: body.phone,
                Address: body.address,
                CreatedOn: date,
            });
            // Saving the company
            newCompany.save().then(saveData => {
                // If saved successfully
                if (saveData) {
                    return response.status(201).json(
                        {
                            status: 201,
                            message: httpMessage[201],
                            count: 1,
                            data: saveData,
                        }
                    );
                }
                // If saved data isn't available or saving failed
                return response.status(500).send(httpMessage[500]);
            }).catch(saveError => {
                LOGGER.error(saveError);
                return response.status(500).send(httpMessage[500]);
            });
        }).catch(findError => {
            LOGGER.error(findError);
            return response.status(503).send(httpMessage[503]);
        });
    } catch (error) {
        LOGGER.error(error);
        next(error);
    }
};

exports.editCompanyById = (request, response, next) => {
    try {
        // Checking if method is allowed
        if( !(request.route.methods.put) || request.method != "PUT" ) {
            return response.status(405).send(httpMessage[405]);
        }
        // Request body
        let body = request.body;
        // Request parameters
        let params = request.params;
        // Validation
        if (body == null || body == "" || body == undefined) {
            return response.status(400).send(httpMessage[400]);
        }
        if ((body.firstName == ""|| body.firstName == null || body.firstName == undefined) || 
          (body.email == ""|| body.email == null || body.email == undefined) ||
          (params.id == "" || params.id == null || params == undefined)) {
            return response.status(400).send(httpMessage[400]);
        }
        // Checking if email is taken
        companyModel.findOne({Email: body.email}).then(findData => {
            if (findData || findData != (null||""||undefined)) {
                if (findData._id != params.id) {
                    return response.status(409).send(httpMessage[409]);
                }
            }
            // Updating company by id
            companyModel.findByIdAndUpdate(params.id,
                {
                    // Updating data
                    Firstname: body.firstName,
                    Lastname: body.lastName,
                    Email: body.email,
                    Phone: body.phone,
                    Address: body.address,
                    UpdatedOn: new Date(),

                }, {new: true})
            .then(saveData => {
                if (saveData || saveData != (null||""||undefined)) {
                    return response.status(201).json(
                        {
                            status: 201,
                            message: httpMessage[201],
                            count: 1,
                            data: saveData,
                        }
                    );
                }
                return response.status(404).send(httpMessage[404]);
            }).catch(saveError => {
                LOGGER.error(saveError);
                return response.status(503).send(httpMessage[503]);
            });
        }).catch(findError => {
            LOGGER.error(findError);
            return response.status(503).send(httpMessage[503]);
        });
    } catch (error) {
        LOGGER.error(error);
        next(error);
    }
};

exports.getCompanyById = (request, response, next) => {
    try {
        // Checking if method is allowed
        if( !(request.route.methods.get) || request.method != "GET" ) {
            return response.status(405).send(httpMessage[405]);
        }
        // Request parameters
        let params = request.params;
        if (params.id == null || params.id == "" || params.id == undefined) {
            return response.status(400).send(httpMessage[400]);
        }
        // Finding company by id
        companyModel.findById(params.id).then(findData => {
            // If company not found
            if (!findData) {
                return response.status(404).send(httpMessage[404]);
            }
            // Return company details
            return response.status(200).json(
                {
                    status: 200,
                    message: httpMessage[200],
                    count: 1,
                    data: findData,
                }
            );
        }).catch(findError => {
            LOGGER.error(findError);
            return response.status(503).send(httpMessage[503]);
        });
    } catch (error) {
        LOGGER.error(error);
        next(error);
    }
};

exports.deleteCompanyById = (request, response, next) => {
    try {
        // Checking if method is allowed
        if( !(request.route.methods.delete) || request.method != "DELETE" ) {
            return response.status(405).send(httpMessage[405]);
        }
        // Request parameters
        let params = request.params;
        if (params.id == null || params.id == "" || params.id == undefined) {
            return response.status(400).send(httpMessage[400]);
        }
        companyModel.findByIdAndDelete(params.id).then(findData => {
            if (!findData) {
                return response.status(404).send(httpMessage[404]);
            }
            return response.status(200).json(
                {
                    status: 200,
                    message: httpMessage[200],
                }
            );
        }).catch(findError => {
            LOGGER.error(findError);
            return response.status(503).send(httpMessage[503]);
        });
    } catch (error) {
        LOGGER.error(error);
        next(error);
    }
};

exports.listAllCompanies = (request, response, next) => {
    try {
        let query = request.query;
        // Page & limit
        let page = 0; let limit = 10;
        // null query
        let filterQuery = {};
        if (query) {
            if(query.page){
                query.page = parseInt(query.page);
                page = Number.isInteger(query.page)?query.page:page;
            }
            if(query.limit){
                limit = (query.limit <= 30)?parseInt(query.limit):limit;
            }
            // Filter conditions
            // filterQuery.Firstname = new RegExp(query.filter_value);
            if (query.filter_name && query.filter_value) {
                switch (query.filter_name) {
                    case 'Firstname':
                        filterQuery = { "Firstname": new RegExp(query.filter_value, 'i') };
                        break;
                    case 'Lastname':
                        filterQuery = { "Lastname": new RegExp(query.filter_value, 'i') };
                        break;
                    case 'Email':
                        filterQuery = { "Email": new RegExp(query.filter_value, 'i') };
                        break;
                    case 'CreatedOn':
                        filterQuery = { "CreatedOn": new RegExp(query.filter_value, 'i') };
                        break;
                    default:
                        filterQuery = { "Firstname": new RegExp(query.filter_value, 'i') };
                }
            }
            // filterQuery = (query.filter_name && query.filter_value)?{"Firstname": new RegExp(query.filter_value)}:filterQuery;
        }
        //Getting total company count
        companyModel.countDocuments(filterQuery).then(totalCount => {
            // Filtering or getting the list of companies according to conditions
            companyModel.find(filterQuery).limit(limit).skip(page*limit).sort({'_id': -1}).exec().then(findData => {
                return response.status(200).json(
                    {
                        status: 200,
                        message: httpMessage[200],
                        count: totalCount,
                        data: findData,
                    }
                );
            }).catch(findError => {
                LOGGER.error(findError);
                return response.status(500).send(httpMessage[500]);
            });
        }).catch(countError => {
            LOGGER.error(countError);
            return response.status(503).send(httpMessage[503]);
        });
    } catch (error) {
        LOGGER.error(error);
        next(error);
    }
};