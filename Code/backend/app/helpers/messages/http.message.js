let httpCodeMessages = [];

/*---------Successful responses-----------*/
httpCodeMessages[200] = 'OK Success';
httpCodeMessages[201] = 'Created resource';
httpCodeMessages[202] = 'Accepted';
httpCodeMessages[204] = 'No Content';

/*---------Client error responses----------*/
httpCodeMessages[400] = 'Bad request';
httpCodeMessages[401] = 'Unauthorized access';
httpCodeMessages[402] = 'Payment required';
httpCodeMessages[403] = 'Forbidden';
httpCodeMessages[404] = 'Page not found';
httpCodeMessages[405] = 'Method not allowed';
httpCodeMessages[408] = 'Request timeout';
httpCodeMessages[409] = 'Server conflict';
httpCodeMessages[410] = 'Requested content permanently deleted';
httpCodeMessages[414] = 'URI too long';
httpCodeMessages[418] = `I'm a teapot`;
httpCodeMessages[429] = 'Too many requests';

/*----------Server error responeses---------*/
httpCodeMessages[500] = 'Internal server error';
httpCodeMessages[501] = 'Requested method not supported';
httpCodeMessages[502] = 'Bad Getway';
httpCodeMessages[503] = 'service unavailble';
httpCodeMessages[504] = 'Getway timeout';
httpCodeMessages[505] = 'HTTP Version not supported';
httpCodeMessages[507] = 'Insufficient storage';
httpCodeMessages[508] = 'The server detected an infinite loop while processing the request';
httpCodeMessages[511] = 'Network authentication required';

module.exports = httpCodeMessages;