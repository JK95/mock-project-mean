const mongoose = require('mongoose');

let Schema = mongoose.Schema;
let mongooseType = mongoose.Schema.Types;

let companySchema = new Schema({
    Firstname: {
        type: mongooseType.String,
        required: true,
    },
    Lastname: {
        type: mongooseType.String,
    },
    Email: {
        type: mongooseType.String,
        required: true,
    },
    Phone: {
        type: mongooseType.String,
    },
    Address: {
        type: mongooseType.String,
    },
    CreatedOn: {
        type: mongooseType.String,
        required: true,
    },
    UpdatedOn: {
        type: mongooseType.Date,
    },
});
module.exports = mongoose.model('Company', companySchema);