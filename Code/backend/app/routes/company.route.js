module.exports = (app) => {
    const companyController = require('../controllers/company.controller');

    // Add company
    app.post('/company', companyController.addCompany);
    // Edit company by id
    app.put('/company/:id?', companyController.editCompanyById);
    // Get company by id
    app.get('/company/:id?', companyController.getCompanyById);
    // List companies
    app.get('/list-companies', companyController.listAllCompanies);
    // Delete company by id
    app.delete('/company/:id?', companyController.deleteCompanyById);
}