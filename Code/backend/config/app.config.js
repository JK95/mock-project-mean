// const UIDGenerator = require('uid-generator');
// const uidgen = new UIDGenerator();
//Sync
// let uid = uidgen.generateSync(); // unique id

module.exports = {
    db: {
        host: process.env.Database,
    },
    session : {
        Key : '20201510',
        Round: process.env.SessionRound,
    },
    server: {
        domain: process.env.ServerDomain,
        port: process.env.ServerPort,
    },
    mailer : {
        user : process.env.ServerMailAccount,
        pass : process.env.ServerMailPassword,
    },
}