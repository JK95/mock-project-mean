const SimpleNodeLogger = require('simple-node-logger'),
    opts = {
        logFilePath: process.env.LogFile,
        timestampFormat: 'YYYY-MM-DD hh:mm:ss.A'
    },
log = SimpleNodeLogger.createSimpleLogger( opts );
module.exports = log;