const path = require('path');
require('dotenv').config({
    path: path.join(__dirname, '.env'),
});
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
// const fs = require('fs');
const appConfig = require('./config/app.config');
const LOGGER = require('./config/project.logger');
// const errorPage = require('./app/helpers/templates/page.template');

// create an express app 
const app = express();

// app.use('/file', express.static(path.join(__dirname, 'uploads/')));

app.use(cors({
    origin: (origin, cb)=>{
        cb(null, true);
    },
    credentials: true,
}));

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true, limit: '500mb' }));

// parse requests of content-type - application/json
app.use(bodyParser.json());

app.use(cookieParser());

//--------------------------------------------------------//
// Configuring the database
const mongoose = require('mongoose');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

mongoose.Promise = global.Promise;

// Connecting to the database
const db = appConfig.db.host;
mongoose.connect(db, {
    useNewUrlParser: true
}).then(() => {
    LOGGER.info(`Successfully connected to database`);
}).catch( error => {
    LOGGER.error(`Failed to connect: ${error}`);
    process.exit();
});
//--------------------------------------------------------//

// define a simple route
app.get('/api', (req, res) => {
    return res.status(200).send('API works fine');
    // fs.exists(path.join(__dirname, 'app/api.html'),fileExists => {
    //     if (fileExists) {
    //         return res.status(200).sendFile('app/api.html', {root: __dirname});
    //     } else {
    //         return res.status(404).send(errorPage.NotFound);
    //     }
    // });
});

// Require company routes
require('./app/routes/company.route')(app);
/*
// Error handler
app.use(function(error, request, response, next){
    // Set locals, only providing error in development
    response.locals.message = error.message;
    response.locals.error = request.app.get('env') === 'development' ? error:{};
    // Render the error page
    response.status(error.status||500);
    response.render('error');
});
*/
// listen for requests
const server_port = appConfig.server.port;
const domain = appConfig.server.domain;
app.listen(server_port, () => {
    LOGGER.info(`Server listening on ${domain}:${server_port}`);
});