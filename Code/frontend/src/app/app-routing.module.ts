import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyaddComponent } from './components/companyadd/companyadd.component';
import { CompanyeditComponent } from './components/companyedit/companyedit.component';
import { CompanylistComponent } from './components/companylist/companylist.component';

const routes: Routes = [
  {
    path: '', redirectTo: '/company-list', pathMatch: 'full'
  },
  {
    path: 'company-list', component: CompanylistComponent
  },
  {
    path: 'company-add', component: CompanyaddComponent
  },
  {
    path: 'company-edit/:id', component: CompanyeditComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
