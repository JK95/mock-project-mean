import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CompanylistComponent } from './components/companylist/companylist.component';
import { CompanyaddComponent } from './components/companyadd/companyadd.component';
import { CompanyeditComponent } from './components/companyedit/companyedit.component';

@NgModule({
  declarations: [
    AppComponent,
    CompanylistComponent,
    CompanyaddComponent,
    CompanyeditComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    NgxPaginationModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
