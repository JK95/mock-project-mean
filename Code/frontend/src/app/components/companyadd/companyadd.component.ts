import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { error } from 'protractor';
import { CompanyService } from 'src/app/services/company/company.service';

@Component({
  selector: 'app-companyadd',
  templateUrl: './companyadd.component.html',
  styleUrls: ['./companyadd.component.css']
})
export class CompanyaddComponent implements OnInit {

  constructor(private _router: Router, private _companyService: CompanyService) { }

  ngOnInit(): void {}

  public addCompany(companyForm: any): void
  {
    if(companyForm.valid && companyForm.submitted){
      this._companyService.addCompany(companyForm.value)
      .subscribe((response) => {
        if(response.status === 201){
          this._router.navigate(['/']);
        }else{
          this._showResponse(response.status);
        }
      },(error) => {
        console.log(error);
      });
    }
  }

  private _showResponse(code: number): void
  {
    switch (code) {
      case 409:
        alert('Company already exists');
        break;
      default:
        alert('Service unavailable. try again later.');
        break;
    }
  }
}
