import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CompanyService } from 'src/app/services/company/company.service';

@Component({
  selector: 'app-companyedit',
  templateUrl: './companyedit.component.html',
  styleUrls: ['./companyedit.component.css']
})
export class CompanyeditComponent implements OnInit {

  public companyId: string = this._route.snapshot.paramMap.get('id');
  public companyDetails: any;

  constructor(private _route: ActivatedRoute, private _router: Router, private _companyService: CompanyService) { }

  ngOnInit(): void
  {
    this._companyService.getCompanyById(this.companyId).subscribe((response) => {
      if(response.status === 200){
        this.companyDetails = response.data;
      }
    },(error) => {
      console.log(error);
    });
  }

  public updateById(companyForm: any): void
  {
    if(companyForm.valid && companyForm.submitted){
      this._companyService.updateCompanyById(this.companyId, companyForm.value)
      .subscribe((response) => {
        if(response.status === 201){
          this._router.navigate(['/']);
        }else{
          this._showResponse(response.status);
        }
      },(error) => {
        console.log(error);
      });
    }
  }

  private _showResponse(code: number): void
  {
    switch (code) {
      case 409:
        alert('Company already exists');
        break;
      default:
        alert('Service unavailable. try again later.');
        break;
    }
  }
}