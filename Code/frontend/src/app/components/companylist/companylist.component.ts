import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CompanyService } from 'src/app/services/company/company.service';

@Component({
  selector: 'app-companylist',
  templateUrl: './companylist.component.html',
  styleUrls: ['./companylist.component.css']
})
export class CompanylistComponent implements OnInit {

  public companies: any;
  public totalCompanies: number;
  public page: number = 1;
  public limit: number = 5;
  public filterValues: any;
  public isFilter: boolean = false;

  constructor(private _companyService: CompanyService, private _router: Router) {}

  ngOnInit(): void
  {
    this.listCompanies(this.page);
    this.isFilter = false;
  }

  public filterCompany(filterForm: any): void
  {
    if(filterForm.valid && filterForm.submitted){
      filterForm = filterForm.value;
      this.isFilter = true;
      this.filterValues = filterForm;
      this._companyService.getCompanyList(this.limit, this.page, filterForm.filterName, filterForm.filterValue).subscribe((response) => {
        this.companies = response.data;
        this.totalCompanies = response.count;
      },(error) => {
        console.log(error);
      });
    }else{
      this.listCompanies(1);
      this.isFilter = false;
    }
  }

  public listCompanies(event: number): void
  {
    this.page = event - 1;
    if(!this.isFilter){
      this._companyService.getCompanyList(this.limit, this.page).subscribe((response)=>{
        this.companies = response.data;
        this.totalCompanies = response.count;
      },(error)=>{
        console.log(error);
      });
    }else{
      this._companyService.getCompanyList(this.limit, this.page, this.filterValues.filterName, this.filterValues.filterValue)
      .subscribe((response) => {
        this.companies = response.data;
        this.totalCompanies = response.count;
      },(error) => {
        console.log(error);
      });
    }
  }

  public editById(companyId: string): void
  {
    this._router.navigate(['/company-edit', companyId]);
  }

  public deleteById(companyId: string): void
  {
    if(window.confirm('Continue?')){
      this._companyService.deleteCompanyById(companyId).subscribe((response) => {
        this._showResponse(response.status);
        this.listCompanies(1);
        this.isFilter = false;
      },(error) => {
        console.log(error);
      });
    }
  }

  private _showResponse(code: number): void
  {
    switch (code) {
      case 200:
        alert('Successful');
        break;
      case 409:
        alert('Company already exists');
        break;
      default:
        alert('Service unavailable. try again later.');
        break;
    }
  }
}