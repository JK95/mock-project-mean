import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { env } from 'process';
@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private _http: HttpClient) {}

  public getCompanyList(limit: number=10, page: number=0, filterName: any=null, filterValue: any=null): Observable<any>
  {
    let url = `${environment.apiUrl}list-companies?limit=${limit}&page=${page}`;
    url = (filterName !== null && filterValue !== null)?`${environment.apiUrl}list-companies?filter_name=${filterName}&filter_value=${filterValue}&limit=${limit}&page=${page}`:url;
    return Observable.create(observer => {
      this._http.get(url).subscribe((response: Response)=>{
        observer.next(response);
        observer.complete();
      },(error: Error)=>{
        observer.next(error);
        observer.complete();
      });
    });
  }

  public getCompanyById(companyId: string): Observable<any>
  {
    return Observable.create(observer => {
      this._http.get(`${environment.apiUrl}company/${companyId}`).subscribe((response: Response) => {
        observer.next(response);
        observer.complete();
      },(error: Error) => {
        observer.next(error);
        observer.complete();
      });
    });
  }

  public deleteCompanyById(companyId: string): Observable<any>
  {
    return Observable.create(observer => {
      this._http.delete(`${environment.apiUrl}company/${companyId}`).subscribe((response: Response) => {
        observer.next(response);
        observer.complete();
      },(error: Error) => {
        observer.next(error);
        observer.complete();
      });
    });
  }

  public updateCompanyById(companyId: string, data: any): Observable<any>
  {
    return Observable.create(observer => {
      this._http.put(`${environment.apiUrl}company/${companyId}`,
      JSON.stringify(data), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      })
      .subscribe((response: Response)=>{
        observer.next(response);
        observer.complete();
      },(error: Error)=>{
        observer.next(error);
        observer.complete();
      });
    });
  }

  public addCompany(data: any): Observable<any>
  {
    return Observable.create(observer => {
      this._http.post(`${environment.apiUrl}company`,
      JSON.stringify(data), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      })
      .subscribe((response: Response) => {
        observer.next(response);
        observer.complete();
      },(error: Error) => {
        observer.next(error);
        observer.complete();
      });
    });
  }
}