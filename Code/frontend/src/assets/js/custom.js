$(document).ready(function(){
    $('select#filter-name').on('change', function(){
        let selectedFilterName = $("select#filter-name option:selected").val();
        if(selectedFilterName == 'CreatedOn'){
            $('input#filter-value').addClass('filter-date');
            $('input.filter-date').datetimepicker(
                {
                    format: 'Y/m/d'
                }
            );
        }else{
            $('input#filter-value').removeClass('filter-date');
        }
    });
    $(document).on('click', '#jk-clear-filter', function(){
        $('input#filter-value').removeClass('filter-date');
    });
});